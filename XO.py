﻿import os
import random
import colorama
from colorama import Fore, Back
colorama.init()
import time
import math



class FieldXO:
	X = 0
	Y = 0
	Cells = []
	players = []
	curPlayer = 0

	iter=0

	def __init__(self, x, y, plas):
		self.X = x
		self.Y = y
		self.Cells = []
		self.players = plas

		if(x < 3 or y < 3):
			print("FIELD IS TOO SMALL!!!")
			return

		for i in range(self.Y):
			arr = []
			for j in range(self.X):
				arr.append("#")
			self.Cells.append(arr)

	def UpdateScreen(self, isWin):
		os.system('cls')
		self.iter += 1
		print(Back.BLACK)
		print("X&O")

		rowg = "+ "
		for j in range(self.X):
			rowg += str(j + 1) + " "
		print(Back.GREEN)
		print(rowg)

		print(Back.BLUE, end='')
		for i in range(self.Y):
			print(Back.GREEN + str(i + 1) + " ", end='')
			row = ""
			for j in range(self.X):
				if(self.Cells[i][j] == '#'):
					row+=str(Back.BLACK)
				elif (self.Cells[i][j] == 'X'):
					row+=str(Back.RED)
				else:
					row+=str(Back.BLUE)
				row += self.Cells[i][j] + " "
			print(row)

		if not isWin:
			if(self.curPlayer==0):
				print(Back.RED, end="")
			else:
				print(Back.BLUE, end="")
			print("Player " + self.players[self.curPlayer]*3 + " turn!")
			print("Input: <x> <y>")
			self.Input()

	def Input(self):
		s = input().split()
		self.Turn(int(s[0])-1, int(s[1])-1)

	def Turn(self, x, y):
		if(self.Cells[y][x] != "#"):
			print("WRONG!")
			self.Input()
			return
		self.Cells[y][x] = self.players[self.curPlayer]
		if self.Check():
			self.UpdateScreen(True)
			print(self.players[self.curPlayer] + "! YOU WON!")
			return

		self.curPlayer = (self.curPlayer+1) % 2
		self.UpdateScreen(False)

	def Check(self):
		for i in range(self.Y):
			nInRow = 0
			for j in range(self.X):
				if(self.Cells[i][j] != self.players[self.curPlayer]):
					nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True

		for i in range(self.X):
			nInRow = 0
			for j in range(self.Y):
				if(self.Cells[j][i] != self.players[self.curPlayer]):
					nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True

		for j in range(self.Y):
			nInRow = 0
			i = 0
			while(self.isInField(i, i+j)):
				if(self.Cells[i+j][i] != self.players[self.curPlayer]):
						nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True
				i+=1
		for j in range(1, self.X):
			nInRow = 0
			i = 0
			while(self.isInField(i+j, i)):
				if(self.Cells[i][i+j] != self.players[self.curPlayer]):
						nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True
				i+=1
		for j in range(self.Y):
			nInRow = 0
			i = 0
			while(self.isInField(-i-1, i+j)):
				if(self.Cells[i+j][-i-1] != self.players[self.curPlayer]):
						nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True
				i+=1
		for j in range(0, self.X-1):
			nInRow = 0
			i = 0
			while(self.isInField(j-i, i)):
				if(self.Cells[i][j-i] != self.players[self.curPlayer]):
						nInRow = 0
				else:
					nInRow+=1
					if nInRow >= 3:
						return True
				i+=1
		
		return False

	def isInField(self, x, y):
		if self.X > x and self.Y > y and -(self.X)-1<x and -(self.Y)-1<y:
			return True
		else:
			return False





f = FieldXO(3, 3, ["X", "O"])
f.UpdateScreen(False)